# Users schema
 
# --- !Ups
 
CREATE TABLE post (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    fb_id varchar(255) NOT NULL,
    created_time DATETIME NOT NULL,
    message TEXT NOT NULL,
    likes_count INT NOT NULL,
    name varchar(50) NOT NULL,
    PRIMARY KEY (id)
);
 
# --- !Downs
 
DROP TABLE post;