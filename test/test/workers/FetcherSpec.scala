package test.workers

import org.specs2.mutable.Specification
import actors.FetchActor
import actors.FetchActor
import models.Post
import play.api.test.Helpers._
import play.api.test.FakeApplication
import akka.testkit.TestActorRef
import scala.concurrent.duration._
import scala.concurrent.Await
import akka.pattern.ask
import akka.testkit.TestKit
import akka.actor.ActorSystem
import org.joda.time.DateTime

class FetcherSpec extends TestKit(ActorSystem("test")) with Specification {

  "Fetcher" should {
    "fetch data and search test" in {
      running(FakeApplication()) {

        val actorRef = TestActorRef(new FetchActor)
        val actorRefReal = actorRef.underlyingActor
        
        actorRefReal.fetchAll("283043588499698", DateTime.now.minusHours(2), true);
        assert(Post.getSize() > 0);

        val result = Post.search("홍대", 0, 2)
        assert(result.size > 0);

        result foreach { r => println(r) }
      }
    }
  }

}