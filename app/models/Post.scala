package models

import anorm._
import anorm.SqlParser._
import play.api.db._
import org.joda.time.DateTime
import java.util.Date
import play.api.Logger

/**
 * PK[Long]가 이것저것 귀찮으면 그냥 Long으로 하고,
 * parser에도 Long으로 설정해주는 방법도 있음 (대신 모든 Query는 raw로 해야함)
 */
case class Post(
  id: Pk[Long] = NotAssigned,
  fbId: String,
  createdTime: Date,
  message: String,
  likesCount: Int,
  name: String) {

  // special quries should be here
  // @See also https://gist.github.com/guillaumebort/2788715
}

object Post {

  import play.api.Play.current

  val parser = {
    (
      get[Pk[Long]]("id") ~
      get[String]("fb_id") ~
      get[Date]("created_time") ~
      get[String]("message") ~
      get[Int]("likes_count") ~
      get[String]("name"))
      .map {
        case id ~ fbId ~ createdTime ~ message ~ likesCount ~ name =>
          Post(id, fbId, createdTime, message, likesCount, name)
      }
  }

  def getSize(): Long =
    DB.withConnection { implicit c =>
      SQL("""
          SELECT count(id) FROM post p
          """)
        .as(scalar[Long].single)
    }

  def search(keyword: String, offset: Int, limit: Int) = {
    // deadly slow, should be improved near future
    DB.withConnection { implicit c =>
      SQL("""
        SELECT * FROM post p 
        WHERE p.message like {keyword} 
        LIMIT {limit} 
        OFFSET {offset} 
        """)
        .on("keyword" -> ("%" + keyword + "%"),
          "offset" -> offset,
          "limit" -> limit)
        .list(parser)
    }
  }

  def insertOrUpdate(post: com.restfb.types.Post) = {
    DB.withConnection { implicit c =>
      val existId = SQL("""
              SELECT id FROM post p WHERE p.fb_id = {fbId}
              """)
        .on("fbId" -> post.getId)
        .as(scalar[Long].singleOpt)

      if (existId == None) {
        SQL(
          """
	        INSERT INTO post 
        	  (fb_id, created_time, message, likes_count, name) 
            VALUES 
	          ({fbId}, {createdTime}, {message}, {likesCount}, {name})
	              """)
          .on('fbId -> post.getId,
            'createdTime -> post.getCreatedTime,
            'message -> (
              if (post.getMessage == null) ""
              else post.getMessage),
            'likesCount -> (
              if (post.getLikesCount == null) 0
              else post.getLikesCount),
            'name -> post.getFrom.getName)
          .executeInsert()
      } else {

        Logger.info("Update id = %s".format(post.getId))
        SQL(
          """
            UPDATE post 
        	  SET message = {message}, likes_count = {likesCount}
        	  WHERE id = {id}
	      """)
          .on('id -> existId,
            'message -> (
              if (post.getMessage == null) ""
              else post.getMessage),
            'likesCount -> (
              if (post.getLikesCount == null) 0
              else post.getLikesCount))
          .executeUpdate()
      }
    }
  }
}