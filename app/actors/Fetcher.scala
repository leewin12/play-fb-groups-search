package actors

import org.joda.time.DateTime
import com.restfb.DefaultFacebookClient
import com.restfb.Parameter
import com.restfb.json.JsonObject
import com.restfb.types.Post
import anorm.SQL
import anorm.sqlToSimple
import anorm.toParameterValue
import play.api.Play.current
import play.api.db.DB
import play.api.Play
import play.api.Logger
import anorm.SqlParser._
import anorm.Row
import akka.actor.ActorLogging
import akka.actor.Actor
import akka.event.Logging

case class Fetch(groupId: String)

class FetchActor extends Actor with ActorLogging {

  val TOKEN = Play.application.configuration.getString("fb.token")
  val fbClient = new DefaultFacebookClient(TOKEN.get);

  def receive = {
    null
//    case Fetch(groupId) => {
//      log.info("trigger fetch, groupId = ?".format(groupId))
//      fetchAll(groupId, DateTime.now.minusYears(1))
//    }
  }

  def fetchAll(groupId: String, lastFetchDate: DateTime) {
    fetchAll(groupId, lastFetchDate, false)
  }

  // 283043588499698/feed?fields=id,created_time,message,comments.fields(from),name,updated_time,likes&limit=25
  // 맛스타그룹 group id = 283043588499698
  /**
   * isOnce: 유닛테스트용으로서, true=딱 한번만 작동, false=recv로 작동
   */
  def fetchAll(groupId: String, lastFetchDate: DateTime, isOnce: Boolean) {

    val limit = 50
    val url = "%s/feed".format(groupId)
    val mapper = fbClient.getJsonMapper()
    var fetchSize = 0

    fetchPost(url)

    def fetchPost(url: String) {

      // Parameter.`with` 임에 주의, 
      // 이게 scala reserved keyword와 충돌시 사용하는 방법
      val rawObj = fbClient.fetchObject(url, classOf[JsonObject],
        Parameter.`with`("limit",
          limit),
        Parameter.`with`("fields",
          "id,created_time,message,updated_time,likes,from,actions,comments"));
      val rawPosts = mapper.toJavaList(rawObj.getString("data"), classOf[Post]);
      val rawUrl = rawObj.getJsonObject("paging").getString("next");

      log.info("rawNextUrl = " + rawUrl)

      var itor = rawPosts.iterator;
      while (itor.hasNext) {
        val post = itor.next;
        //        Console.println(
        //          post.getId, post.getCreatedTime,
        //          post.getLikesCount, post.getFrom.getName)

        models.Post.insertOrUpdate(post)

        // TODO fetch comments

        fetchSize += 1;
      }

      log.info("fetchSize = " + fetchSize)

      if (!isOnce && rawPosts.size >= (limit / 2)) {
        val nextUrl = rawUrl.substring(rawUrl.indexOf(groupId));
        log.info("fetchSize = " + fetchSize + ", next = " + nextUrl);

        Thread.sleep(2000)
        fetchPost(nextUrl)
      }
    }

  }

  def fetchComment(groupId: String, postId: String, url: String) {

  }

}