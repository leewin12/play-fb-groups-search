

import scala.concurrent.duration.DurationInt
import actors.FetchActor
import akka.actor.Props
import play.api._
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits._
import actors.Fetch

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    val fetcher = Akka.system.actorOf(Props[FetchActor], name = "fetcher")
    Akka.system.scheduler.schedule(0 seconds, 30 minutes,
      fetcher, Fetch("283043588499698"))
  }

  override def onStop(app: Application) {
    Logger.info("Application shutdown...")
    Akka.system.shutdown
  }

}