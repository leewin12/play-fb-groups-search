package controllers

import anorm._
import models.Post
import play.api._
import play.api.libs.json.Format
import play.api.libs.json.JsNumber
import play.api.libs.json.JsValue
import play.api.mvc._
import play.api.libs.json.JsResult
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsNull
import play.api.libs.json.Json
import play.api.libs.json.Writes
import play.api.libs.json.JsObject
import play.api.libs.json.JsArray
import play.api.libs.json.JsArray

object Search extends Controller {

  /**
   * @See http://bryangilbert.com/code/2013/07/01/anorm-pk-json/
   */
  implicit object PkFormat extends Format[Pk[Long]] {
    def reads(json: JsValue): JsResult[Pk[Long]] = JsSuccess(
      json.asOpt[Long].map(id => Id(id)).getOrElse(NotAssigned))
    def writes(id: Pk[Long]): JsValue = id.map(JsNumber(_)).getOrElse(JsNull)
  }

  implicit val postFormat = Json.format[Post]

  implicit object SearchResultFormat extends Writes[Seq[Post]] {
    def writes(posts: Seq[Post]) = Json.toJson(
      posts.map { x => postFormat.writes(x) })
  }

  def search(keyword: String, offset: Option[Int], limit: Option[Int]) = Action {

    implicit req =>

      val results = Post.search(keyword, offset.getOrElse(0), limit.getOrElse(10))

      render {
        case Accepts.Json => {
          Ok(SearchResultFormat.writes(results))
        }
        case _ => {
          Ok(views.html.search_result(results, keyword))
        }
      }
  }

  case class TestObj(name: String, msg: String)

  def insertMulti() = Action {
    implicit req =>
      // parse posted json objs
      val json = req.body.asJson.getOrElse(JsNull)
      val objs = json.as[List[JsValue]].map {
        json => Json.reads[TestObj].reads(json).get
      }
      // return what we got as json string
      Ok(JsArray(objs map {
        obj => Json.writes[TestObj].writes(obj)
      }))
  }

  def insertSingle() = Action {
    implicit req =>
      // parse posted json obj
      val obj = Json.reads[TestObj].reads(req.body.asJson.getOrElse(JsNull))
      // return what we got as json string
      Ok(Json.writes[TestObj].writes(obj.getOrElse(null)))
  }

}