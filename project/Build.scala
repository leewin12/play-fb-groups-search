import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  // to update dependencies (in play console)
  // > reload
  // > update
  // > eclipse
  
  val appName = "fb-sentinel"
  val appVersion = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    jdbc,
    anorm,
    "com.restfb" % "restfb" % "1.6.12",
    "com.typesafe.akka" %% "akka-testkit" % "2.1.0" % "test")

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // default maven central이 자동으로 안들어가 있는듯
    resolvers ++= Seq("mvn central" at "http://repo1.maven.org/maven2",
        "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"))

}
